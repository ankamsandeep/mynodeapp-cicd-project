import os
import requests

GITLAB_URL = "https://gitlab.com/ankamsandeep/mynodeapp-cicd-project"
PRIVATE_TOKEN = "glpat-9s3aPqFU1smmHxCzTWKh"
PROJECT_ID = "42930219"
IMAGE_FILE_NAME = "registry.gitlab.com/ankamsandeep/mynodeapp-cicd-project"
WIKI_PAGE_TITLE = "automate"

def get_image_url(project_id, image_file_name):
    response = requests.get(
        f"{GITLAB_URL}/api/v4/projects/{project_id}/registry/repositories",
        headers={
            "Private-Token": PRIVATE_TOKEN
        }
    )

    for repo in response.json():
        if repo["path"] == image_file_name:
            return repo["location"]

    raise ValueError(f"Image file '{image_file_name}' not found in project {project_id}")

def update_wiki_page(project_id, private_token, image_file_name, wiki_page_title):
    image_url = get_image_url(project_id, image_file_name)

    response = requests.get(
        f"{GITLAB_URL}/api/v4/projects/{project_id}/wikis",
        headers={
            "Private-Token": private_token
        }
    )

    for wiki in response.json():
        if wiki["title"] == wiki_page_title:
            wiki_slug = wiki["slug"]
            break
    else:
        raise ValueError(f"Wiki page '{wiki_page_title}' not found in project {project_id}")

    response = requests.put(
        f"{GITLAB_URL}/api/v4/projects/{project_id}/wikis/{wiki_slug}",
        headers={
            "Private-Token": private_token
        },
        json={
            "content": f"![image]({image_url})"
        }
    )

    if response.status_code != 200:
        raise ValueError(f"Failed to update wiki page '{wiki_page_title}' in project {project_id}: {response.text}")

if __name__ == "__main__":
    update_wiki_page(PROJECT_ID, PRIVATE_TOKEN, IMAGE_FILE_NAME, WIKI_PAGE_TITLE)
