
import requests
import datetime

# Replace with your own GitLab API token
api_token = "glpat-9s3aPqFU1smmHxCzTWKh"

# Replace with the ID of your GitLab repository
repo_id = 42930219

# Set the title of the wiki page to include the current date
title = "Container Image Link - " + str(datetime.datetime.now().date())

# Set the content of the wiki page to the link to the container image
content = "https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/container_registry/3846570"

# Define the API endpoint for creating a new wiki page in the repository
url = f"https://gitlab.com/api/v4/projects/{repo_id}/wikis"

# Set the header for the API request to include the API token
headers = {
    "Private-Token": api_token
}

# Set the data for the API request to include the title and content of the wiki page
data = {
    "title": title,
    "content": content
} 

# Send the API request to create the new wiki page
response = requests.post(url, headers=headers, data=data)

# Check the status code of the API response to verify that the wiki page was created successfully
if response.status_code == 201:
    print("Wiki page created successfully")
else:
    print("Failed to create wiki page")
