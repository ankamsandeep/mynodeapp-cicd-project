#!/bin/bash

# Replace these placeholders with your actual values
PROJECT_ID=42930219
PRIVATE_TOKEN=glpat-9s3aPqFU1smmHxCzTWKh

# Get the current running pipeline ID
CURRENT_PIPELINE=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines?status=running" | jq '.[0].id')

# Get the IDs and stage names of all the running jobs for the pipeline that match the job names "test" and "build"
RUNNING_JOBS=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/pipelines/$CURRENT_PIPELINE/jobs?status=running" | jq '[.[] | select(.name | contains("test") or contains("build")) | {id, stage}]')

# Loop over all the running jobs and generate links to their artifacts
LINKS=""
while read -r JOB; do
    JOB_ID=$(echo "$JOB" | jq -r '.id')
    STAGE_NAME=$(echo "$JOB" | jq -r '.stage')
    ARTIFACTS_LINK="[Artifacts for Job $JOB_ID in Stage $STAGE_NAME](https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/-/jobs/$JOB_ID/artifacts/browse)"
    LINKS="$LINKS $ARTIFACTS_LINK<br>"
done <<< "$RUNNING_JOBS"

# Get the current date and time
DATE_TIME=$(date "+%Y-%m-%d %H:%M:%S")

# Get the current content of the wiki page
WIKI_CONTENT=$(curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home" | jq -r '.content')

# Append the links to the wiki content and update the wiki page
NEW_WIKI_CONTENT="$WIKI_CONTENT<br><br>Links to current artifacts as of $DATE_TIME:<br><br>$LINKS<br><br>"
curl --request PUT --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" --data "content=$NEW_WIKI_CONTENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/wikis/home"
