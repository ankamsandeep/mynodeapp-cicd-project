import requests
import datetime

# Replace with your own GitLab API token
api_token = "glpat-9s3aPqFU1smmHxCzTWKh"

# Replace with the ID of your GitLab repository
repo_id = 42930219

# Set the header for the API request to include the API token
headers = {
    "Private-Token": api_token
}

# Define the API endpoint for getting the list of container image tags in the repository
tags_url = f"https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/{repo_id}/container_registry/3846570"
#tags_url = f"https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/{repo_id}/container_registry/tags"
#https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/container_registry/3846570"
# Send the API request to get the list of container image tags
tags_response = requests.get(tags_url, headers=headers)

# Check the status code of the API response to verify that the tags were retrieved successfully
if tags_response.status_code == 200:
    try:
        # Get the list of tags from the API response
        tags = tags_response.json()
    except ValueError:
        print("Error parsing response content as JSON")
        print(f"Response content: {tags_response.content}")
        exit()

    # Get the link to the newest container image tag
    newest_tag_link = f"https://gitlab.com/{repo_id}/container_registry/tags/{tags[0]['name']}"

    # Set the title of the wiki page to include the current date
    title = "Container Image Link - " + str(datetime.datetime.now().date())

    # Define the API endpoint for creating a new wiki page in the repository
    wiki_url = f"https://gitlab.com/api/v4/projects/{repo_id}/wikis"

    # Set the data for the API request to include the title and content of the wiki page
    data = {
        "title": title,
        "content": newest_tag_link
    }

    # Send the API request to create the new wiki page
    wiki_response = requests.post(wiki_url, headers=headers, data=data)

    # Check the status code of the API response to verify that the wiki page was created successfully
    if wiki_response.status_code == 201:
        print("Wiki page created successfully")
    else:
        print("Failed to create wiki page")
else:
    print("Failed to retrieve tags")
