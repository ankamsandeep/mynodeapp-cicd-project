import requests
import datetime

GITLAB_URL = "https://gitlab.com/ankamsandeep/mynodeapp-cicd-project"
PRIVATE_TOKEN = "glpat-9s3aPqFU1smmHxCzTWKh"
PROJECT_ID = "42930219"
IMAGE_FILE_NAME = "https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/container_registry/3846570"

def get_image_url(project_id, image_file_name):
    response = requests.get(f"{GITLAB_URL}/api/v4/projects/{project_id}/container_registry/repositories",
                            headers={"PRIVATE-TOKEN": PRIVATE_TOKEN})
    if response.status_code == 200:
        for repo in response.json():
            if repo['path'] == image_file_name:
                return repo['location']
    else:
        raise Exception(f"Failed to get image URL, response code: {response.status_code}, response: {response.text}")

def create_wiki_page(project_id, private_token, image_file_name, page_title, page_content):
    image_url = get_image_url(project_id, image_file_name)
    data = {
        "title": page_title,
        "content": page_content + "\n" + image_url
    }
    response = requests.post(f"{GITLAB_URL}/api/v4/projects/{project_id}/wikis", headers={"PRIVATE-TOKEN": private_token}, json=data)
    if response.status_code != 201:
        raise Exception(f"Failed to create wiki page, response code: {response.status_code}, response: {response.text}")

if __name__ == '__main__':
    now = datetime.datetime.now()
    WIKI_PAGE_TITLE = now.strftime("%Y-%m-%d")
    WIKI_PAGE_CONTENT = "Wiki page created by automation"
    create_wiki_page(PROJECT_ID, PRIVATE_TOKEN, IMAGE_FILE_NAME, WIKI_PAGE_TITLE, WIKI_PAGE_CONTENT)
