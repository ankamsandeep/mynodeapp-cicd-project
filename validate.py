def get_image_url(project_id, image_file_name):
    url = f"{GITLAB_URL}/api/v4/projects/{project_id}/registry/repositories"
    response = requests.get(url, headers=HEADERS)
    if response.status_code != 200:
        raise Exception("Failed to get repository information")
    for repo in response.json():
        if repo['path'] == image_file_name:
            return repo['location']['url']
    raise Exception("Image repository not found")

def update_wiki_page(project_id, private_token, image_file_name, wiki_page_title):
    image_url = get_image_url(project_id, image_file_name)
    url = f"{GITLAB_URL}/api/v4/projects/{project_id}/wikis/{wiki_page_title}"
    response = requests.get(url, headers=HEADERS)
    if response.status_code != 200:
        raise Exception("Failed to get wiki page information")
    wiki_page = response.json()
    wiki_page['content'] = wiki_page['content'] + "\n![](" + image_url + ")"
    response = requests.put(url, headers=HEADERS, json=wiki_page)
    if response.status_code != 200:
        raise Exception("Failed to update wiki page")
    print("Wiki page updated successfully with image link")

try:
    update_wiki_page(PROJECT_ID, PRIVATE_TOKEN, IMAGE_FILE_NAME, WIKI_PAGE_TITLE)
except Exception as e:
    print("Error:", e)
