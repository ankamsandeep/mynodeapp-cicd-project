import requests
import json
import datetime

GITLAB_URL = "https://gitlab.com/ankamsandeep/mynodeapp-cicd-project/container_registry/3846570"
PRIVATE_TOKEN = "glpat-9s3aPqFU1smmHxCzTWKh"
PROJECT_ID = "42930219"
IMAGE_FILE_NAME = "registry.gitlab.com/ankamsandeep/mynodeapp-cicd-project"
WIKI_PAGE_TITLE = datetime.datetime.now().strftime("%Y-%m-%d")

def get_image_url(project_id, image_file_name):
    response = requests.get(f"{GITLAB_URL}/{project_id}/registry/repositories")
    try:
        response_json = response.json()
    except json.decoder.JSONDecodeError as e:
        print(f"Error decoding JSON: {e}")
        return None
    for repo in response_json:
        if repo["path"] == image_file_name:
            return repo["location"]["repository_url"]
    return None

def update_wiki_page(project_id, private_token, image_file_name, wiki_page_title):
    image_url = get_image_url(project_id, image_file_name)
    if not image_url:
        return

    headers = {
        "Private-Token": private_token
    }
    response = requests.get(f"{GITLAB_URL}/{project_id}/wikis/{wiki_page_title}", headers=headers)
    if response.status_code == 404:
        # Page does not exist, create it
        data = {
            "content": f"![image]({image_url})",
            "title": wiki_page_title
        }
        response = requests.post(f"{GITLAB_URL}/{project_id}/wikis", headers=headers, data=data)
        if response.status_code != 201:
            print(f"Error creating wiki page: {response.content}")
    else:
        try:
            response_json = response.json()
        except json.decoder.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
            return

        content = response_json["content"]
        content += f"\n\n![image]({image_url})"

        data = {
            "content": content,
            "title": wiki_page_title
        }
        response = requests.put(f"{GITLAB_URL}/{project_id}/wikis/{wiki_page_title}", headers=headers, data=data)
        if response.status_code != 200:
            print(f"Error updating wiki page: {response.content}")

update_wiki_page(PROJECT_ID, PRIVATE_TOKEN, IMAGE_FILE_NAME, WIKI_PAGE_TITLE)
